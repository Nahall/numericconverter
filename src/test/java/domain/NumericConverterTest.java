package domain;

import crosscutting.exceptions.IntegerOutOfRangeException;
import crosscutting.exceptions.InvalidCharsException;
import crosscutting.exceptions.InvalidInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class NumericConverterTest {

  private static final String INVALID_INPUT_BY_CHARS_ERROR =
    "Invalid input, only whole numbers are supported: %s";
  private static final String INVALID_INPUT_MAX_VALUE_EXCEEDED =
    "Invalid input, you have exceeded the supported range: %s";
  private NumericConverter numericConverter;

  @BeforeEach
  void setUp() {
    numericConverter = new NumericConverter();
  }

  @Test
  void numberToLettersInvalidInput() {
    final String expectedValue = "!#$123(/&";
    InvalidCharsException invalidCharsException =
      Assertions.assertThrows(InvalidCharsException.class, () -> {
        numericConverter.toLetters(expectedValue);
      });
    Assertions.assertEquals(String.format(INVALID_INPUT_BY_CHARS_ERROR, expectedValue),
      invalidCharsException.getMessage());
  }


  @Test
  void isPositiveOutOfRangeInput() {
    final String expectedValue = "2147483648";
    IntegerOutOfRangeException integerOutOfRangeException =
      Assertions.assertThrows(IntegerOutOfRangeException.class, () -> {
        numericConverter.toLetters(expectedValue);
      });
    Assertions.assertEquals(String.format(INVALID_INPUT_MAX_VALUE_EXCEEDED, expectedValue),
      integerOutOfRangeException.getMessage());
  }

  @Test
  void isNegativeOutOfRangeInput() {
    final String expectedValue = "-2147483649";
    IntegerOutOfRangeException integerOutOfRangeException =
      Assertions.assertThrows(IntegerOutOfRangeException.class, () -> {
        numericConverter.toLetters(expectedValue);
      });
    Assertions.assertEquals(String.format(INVALID_INPUT_MAX_VALUE_EXCEEDED, expectedValue),
      integerOutOfRangeException.getMessage());
  }

  @Test
  void numberToLettersMinInt() throws InvalidInputException {
    final String expected =
      "Minus two billion one hundred forty seven million four hundred eighty three thousand six " 
        + "hundred and forty eight";
    String actual = numericConverter.toLetters("-2147483648");
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numberToLettersMaxInt() throws InvalidInputException {
    final String expected =
      "Two billion one hundred forty seven million four hundred eighty three thousand six "
        + "hundred and forty seven";
    String actual = numericConverter.toLetters("2147483647");
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numberToLettersZero() throws InvalidInputException {
    final String expected = "Zero";
    String actual = numericConverter.toLetters("0000");
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numberToLettersZerosLeft() throws InvalidInputException {
    final String expected = "One hundred and twenty three";
    String actual = numericConverter.toLetters("0000123");
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numberToLettersThreeDigits() throws InvalidInputException {
    final String expected = "One hundred and twenty three";
    String actual = numericConverter.toLetters("123");
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numberToLettersNegativeNumber() throws InvalidInputException {
    final String expected = "Minus one hundred and twenty three";
    String actual = numericConverter.toLetters("-123");
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numberToLettersThousandNum() throws InvalidInputException {
    final String expected = "One hundred twenty three thousand four hundred and fifty six";
    String actual = numericConverter.toLetters("123456");
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numberToLettersMillionNum() throws InvalidInputException {
    final String expected = "One million one hundred twenty three thousand four hundred and fifty six";
    String actual = numericConverter.toLetters("1123456");
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numberToLettersBillionNum() throws InvalidInputException {
    final String expected =
      "One billion one million one hundred twenty three thousand four hundred and fifty six";
    String actual = numericConverter.toLetters("1001123456");
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numberToLettersPlainBillionNum() throws InvalidInputException {
    final String expected = "One billion ";
    String actual = numericConverter.toLetters("1000000000");
    Assertions.assertEquals(expected, actual);
  }


}