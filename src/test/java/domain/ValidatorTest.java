package domain;

import crosscutting.exceptions.IntegerOutOfRangeException;
import crosscutting.exceptions.InvalidCharsException;
import crosscutting.exceptions.InvalidInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ValidatorTest {

  private static final String INVALID_INPUT_BY_CHARS_ERROR =
    "Invalid input, only whole numbers are supported: %s";
  private static final String INVALID_INPUT_MAX_VALUE_EXCEEDED =
    "Invalid input, you have exceeded the supported range: %s";

  Validator validator = new Validator();

  @Test
  void isValidPositiveInteger() throws InvalidInputException {
    validator.isValidInteger(String.valueOf(Integer.MAX_VALUE));
  }

  @Test
  void isValidNegativeInteger() throws InvalidInputException {
    validator.isValidInteger(String.valueOf(Integer.MIN_VALUE));
  }

  @Test
  void isInvalidInput() {
    final String expectedValue = "!#$123(/&";
    InvalidCharsException invalidCharsException =
      Assertions.assertThrows(InvalidCharsException.class, () -> {
        validator.isValidInteger(expectedValue);
      });
    Assertions.assertEquals(String.format(INVALID_INPUT_BY_CHARS_ERROR, expectedValue),
      invalidCharsException.getMessage());
  }

  @Test
  void isPositiveOutOfRangeInput() {
    final String expectedValue = "2147483648";
    IntegerOutOfRangeException integerOutOfRangeException =
      Assertions.assertThrows(IntegerOutOfRangeException.class, () -> {
        validator.isValidInteger(expectedValue);
      });
    Assertions.assertEquals(String.format(INVALID_INPUT_MAX_VALUE_EXCEEDED, expectedValue),
      integerOutOfRangeException.getMessage());
  }

  @Test
  void isNegativeOutOfRangeInput() {
    final String expectedValue = "-2147483649";
    IntegerOutOfRangeException integerOutOfRangeException =
      Assertions.assertThrows(IntegerOutOfRangeException.class, () -> {
        validator.isValidInteger(expectedValue);
      });
    Assertions.assertEquals(String.format(INVALID_INPUT_MAX_VALUE_EXCEEDED, expectedValue),
      integerOutOfRangeException.getMessage());
  }


}
