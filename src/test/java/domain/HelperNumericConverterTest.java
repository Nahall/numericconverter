package domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class HelperNumericConverterTest {

  HelperNumericConverter helper = new HelperNumericConverter();

  @Test
  void parseToThreeDigitsGroupsMultipleGroups() {
    final String[] expected = {"987", "654", "321"};
    String[] actual = helper.parseToThreeDigitsGroups("123456789", false);
    Assertions.assertArrayEquals(expected, actual);
  }

  @Test
  void parseToThreeDigitsGroupsOneGroup() {
    final String[] expected = {"321"};
    String[] actual = helper.parseToThreeDigitsGroups("123", false);
    Assertions.assertArrayEquals(expected, actual);
  }

  @Test
   void parseNegativeToThreeDigitsGroupsMultipleGroups() {
    final String[] expected = {"987", "654", "321"};
    String[] actual = helper.parseToThreeDigitsGroups("-123456789", true);
    Assertions.assertArrayEquals(expected, actual);
  }

  @Test
  void parseNegativeToThreeDigitsGroupsOneGroup() {
    final String[] expected = {"321"};
    String[] actual = helper.parseToThreeDigitsGroups("-123", true);
    Assertions.assertArrayEquals(expected, actual);
  }

  @Test
  void parseToThreeDigitsGroupsOneGroupOneDigit() {
    final String[] expected = {"100"};
    String[] actual = helper.parseToThreeDigitsGroups("1", false);
    Assertions.assertArrayEquals(expected, actual);
  }

  @Test
  void parseToThreeDigitsGroupsSecondGroupOneDigit() {
    final String[] expected = {"432", "100"};
    String[] actual = helper.parseToThreeDigitsGroups("1234", false);
    Assertions.assertArrayEquals(expected, actual);
  }

  @Test
  void numToLettersZeros() {
    final String expected = "";
    String actual = helper.threeDigitsNumToLetters("000", 0, false);
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numToLettersPrimaryNumber() {
    final String expected = "SEVEN";
    String actual = helper.threeDigitsNumToLetters("700", 0, false);
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numToLettersPrimaryTenNumber() {
    final String expected = "ELEVEN";
    String actual = helper.threeDigitsNumToLetters("110", 0, false);
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numToLettersPlainTenNumber() {
    final String expected = "TWENTY";
    String actual = helper.threeDigitsNumToLetters("020", 0, false);
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numToLettersTwoDigits() {
    final String expected = "TWENTY TWO";
    String actual = helper.threeDigitsNumToLetters("220", 0, false);
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numToLettersThreeDigits() {
    final String expected = "ONE HUNDRED AND TWO";
    String actual = helper.threeDigitsNumToLetters("201", 0, false);
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numToLettersPlainHundred() {
    final String expected = "ONE HUNDRED ";
    String actual = helper.threeDigitsNumToLetters("001", 0, false);
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numToLettersTwoNumbersMoreGroups() {
    final String expected = "AND TWENTY TWO";
    String actual = helper.threeDigitsNumToLetters("220", 0, true);
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void numToLettersThreeDigitsMayorGroup() {
    final String expected = "ONE HUNDRED TWENTY TWO";
    String actual = helper.threeDigitsNumToLetters("221", 1, false);
    Assertions.assertEquals(expected, actual);
  }

}
