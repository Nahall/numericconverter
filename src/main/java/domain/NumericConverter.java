package domain;

import crosscutting.enums.IndicatorNumbersEnum;
import crosscutting.enums.PrimaryNumbersEnum;
import crosscutting.exceptions.InvalidInputException;

public class NumericConverter {

  private HelperNumericConverter helper = new HelperNumericConverter();
  private Validator validator = new Validator();
  private final String MINUS = "Minus ";

  public String toLetters(final String number) throws InvalidInputException {
    String numberAsLetters;

    validator.isValidInteger(number);

    if (inputIsZero(number)) {
      numberAsLetters = handleZeroInput();
    } else {
      numberAsLetters = parseToLetters(number);
    }

    return numberAsLetters;
  }

  private boolean inputIsZero(final String number) {
    return number.matches("-?[0]+");
  }

  private String handleZeroInput() {
    String numberAsLetters;
    numberAsLetters =
      String.valueOf(PrimaryNumbersEnum.getNumberEnum("0")).substring(0, 1).toUpperCase()
        + String.valueOf(PrimaryNumbersEnum.getNumberEnum("0")).substring(1).toLowerCase();
    return numberAsLetters;
  }

  private String parseToLetters(final String number) {
    String threeDigitGroupAsLetters;
    StringBuilder numberAsLetters = new StringBuilder();
    boolean negative;
    String[] splittedNumber;

    negative = isNegative(number);
    splittedNumber = helper.parseToThreeDigitsGroups(number, negative);

    numberAsLetters
      .append(helper.threeDigitsNumToLetters(splittedNumber[0], 0, splittedNumber.length > 1));

    for (int i = 1; i < splittedNumber.length; i++) {
      threeDigitGroupAsLetters = helper.threeDigitsNumToLetters(splittedNumber[i], i, false);
      if (!threeDigitGroupAsLetters.equals("")) {
        numberAsLetters
          .insert(0, threeDigitGroupAsLetters + " " + IndicatorNumbersEnum.getIndicator(i) + " ");
      }
    }

    if (negative) {
      return MINUS + numberAsLetters.toString().toLowerCase();
    } else {
      return numberAsLetters.substring(0, 1) + numberAsLetters.substring(1).toLowerCase();
    }
  }

  private boolean isNegative(final String number) {
    return number.charAt(0) == '-';
  }


}
