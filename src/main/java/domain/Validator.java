package domain;

import crosscutting.exceptions.IntegerOutOfRangeException;
import crosscutting.exceptions.InvalidCharsException;
import crosscutting.exceptions.InvalidInputException;

class Validator {

  private static final String INVALID_INPUT_BY_CHARS_ERROR =
    "Invalid input, only whole numbers are supported: %s";
  private static final String INVALID_INPUT_MAX_VALUE_EXCEEDED =
    "Invalid input, you have exceeded the supported range: %s";

  void isValidInteger(String value) throws InvalidInputException {
    if (!value.matches("-?[0-9]+")) {
      throw new InvalidCharsException(String.format(INVALID_INPUT_BY_CHARS_ERROR, value));
    }
    try {
      Integer.valueOf(value);
    } catch (NumberFormatException e) {
      throw new IntegerOutOfRangeException(String.format(INVALID_INPUT_MAX_VALUE_EXCEEDED, value),
        e);
    }
  }

}
