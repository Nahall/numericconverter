package domain;

import crosscutting.enums.IndicatorNumbersEnum;
import crosscutting.enums.PrimaryNumbersEnum;
import crosscutting.enums.TensNumbersEnum;

class HelperNumericConverter {

  String threeDigitsNumToLetters(String element, int groupNum, boolean moreGroups) {

    StringBuilder convertedElement = new StringBuilder();

    if (element.matches("-?[0]+")) {
      return convertedElement.toString();
    }

    if (element.charAt(2) != '0') {
      convertedElement.append(PrimaryNumbersEnum.getNumberEnum("" + element.charAt(2)))
        .append(" ")
        .append(IndicatorNumbersEnum.getIndicator(IndicatorNumbersEnum.HUNDRED.getIndicator()))
        .append(" ");
      if (element.charAt(1) == '0' && element.charAt(0) == '0') {
        return convertedElement.toString();
      }
    }

    if (moreGroups) {
      convertedElement.append("AND ");
    } else if (groupNum == 0 && element.charAt(2) != '0') {
      convertedElement.append("AND ");
    }

    if (element.charAt(1) == '0') {
      return convertedElement.append(PrimaryNumbersEnum.getNumberEnum("" + element.charAt(0)))
        .toString();
    } else if (element.charAt(1) == '1') {
      return convertedElement.append(PrimaryNumbersEnum.getNumberEnum("1" + element.charAt(0)))
        .toString();
    } else if (element.charAt(0) == '0') {
      return convertedElement.append(TensNumbersEnum.getNumberEnum("" + element.charAt(1)))
        .toString();
    } else {
      return convertedElement.append(TensNumbersEnum.getNumberEnum("" + element.charAt(1)))
        .append(" ")
        .append(PrimaryNumbersEnum.getNumberEnum("" + element.charAt(0))).toString();
    }
  }

  String[] parseToThreeDigitsGroups(String number, boolean negative) {

    number = Integer.toString(Integer.parseInt(number));
    if (negative) {
      number = number.substring(1);
    }
    String[] splitNumber = new StringBuilder(number).reverse().toString().split("(?<=\\G...)");

    if (splitNumber[splitNumber.length - 1].length() < 3) {
      splitNumber[splitNumber.length - 1] = fillWithZero(splitNumber[splitNumber.length - 1]);
    }
    return splitNumber;
  }

  private String fillWithZero(String number) {

    StringBuilder numberBuilder = new StringBuilder(number);
    while (numberBuilder.length() < 3) {
      numberBuilder.append("0");
    }
    return numberBuilder.toString();
  }

}
