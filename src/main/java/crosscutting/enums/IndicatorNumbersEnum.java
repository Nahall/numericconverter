package crosscutting.enums;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public enum IndicatorNumbersEnum {

  HUNDRED(99),
  THOUSAND(1),
  MILLION(2),
  BILLION(3);

  private Integer indicator;

  private static final Map<Integer, IndicatorNumbersEnum> IndicatorNumbersEnumMap;

  static {
    Map<Integer, IndicatorNumbersEnum> map = new LinkedHashMap<>();
    for (IndicatorNumbersEnum instance : IndicatorNumbersEnum.values()) {
      map.put(instance.getIndicator(), instance);
    }
    IndicatorNumbersEnumMap = Collections.unmodifiableMap(map);
  }

  public static IndicatorNumbersEnum getIndicator(Integer indicator) {
    return IndicatorNumbersEnumMap.get(indicator);
  }

  IndicatorNumbersEnum(final Integer indicator) {
    this.indicator = indicator;
  }

  public Integer getIndicator() {
    return indicator;
  }

}
