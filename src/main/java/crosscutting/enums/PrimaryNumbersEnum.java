package crosscutting.enums;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public enum PrimaryNumbersEnum {

  ZERO("0"), ONE("1"), TWO("2"), THREE("3"), FOUR("4"), FIVE("5"), SIX("6"), SEVEN("7"), EIGHT("8"), NINE("9"),
  TEN("10"), ELEVEN("11"), TWELVE("12"), THIRTEEN("13"), FOURTEEN("14"), FIFTEEN("15"), SIXTEEN("16"),
  SEVENTEEN("17"), EIGHTEEN("18"), NINETEEN("19");


  private String number;

  private static final Map<String, PrimaryNumbersEnum> PrimaryNumbersEnumMap;

  static {
    Map<String, PrimaryNumbersEnum> map = new LinkedHashMap<>();
    for (PrimaryNumbersEnum instance : PrimaryNumbersEnum.values()) {
      map.put(instance.getNumber(), instance);
    }
    PrimaryNumbersEnumMap = Collections.unmodifiableMap(map);
  }

  PrimaryNumbersEnum(final String number) {
    this.number = number;
  }

  public static PrimaryNumbersEnum getNumberEnum(String number) {
    return PrimaryNumbersEnumMap.get(number);
  }

  public String getNumber() {
    return number;
  }

}
