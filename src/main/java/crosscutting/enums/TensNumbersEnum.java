package crosscutting.enums;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public enum TensNumbersEnum {

  TWENTY("2"), THIRTY("3"), FORTY("4"), FIFTY("5"), SIXTY("6"), SEVENTY("7"), EIGHTY("8"), NINETY("9");

  private String number;

  private static Map<String, TensNumbersEnum> TensNumbersEnumMap;

  static {
    Map<String, TensNumbersEnum> map = new LinkedHashMap<>();
    for (TensNumbersEnum instance : TensNumbersEnum.values()) {
      map.put(instance.getNumber(), instance);
    }
    TensNumbersEnumMap = Collections.unmodifiableMap(map);
  }

  TensNumbersEnum(final String number) {
    this.number = number;
  }

  public static TensNumbersEnum getNumberEnum(String number) {
    return TensNumbersEnumMap.get(number);
  }

  public String getNumber() {
    return number;
  }
}
