package crosscutting.exceptions;

public class InvalidInputException extends Exception {

  public InvalidInputException(String message){
    super(message);
  }

  public InvalidInputException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
