package crosscutting.exceptions;

public class InvalidCharsException extends InvalidInputException {

  public InvalidCharsException(String message) {
    super(message);
  }
}
