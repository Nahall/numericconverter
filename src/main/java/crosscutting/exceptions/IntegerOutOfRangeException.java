package crosscutting.exceptions;

public class IntegerOutOfRangeException extends InvalidInputException {

  public IntegerOutOfRangeException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
