import crosscutting.exceptions.InvalidInputException;
import domain.NumericConverter;

import java.util.Scanner;

public class Main {

  private static final String INSTRUCTION_MESSAGE = "\nEnter a number or  'e' to exit: ";

  public static void main(String args[]) {

    String input;
    Scanner scn = new Scanner(System.in);
    NumericConverter converter = new NumericConverter();
    printWelcomeMessage();
    input = scn.next();
    while (!input.equals("e")) {
      try {
        System.out.println(converter.toLetters(input));
      } catch (InvalidInputException e) {
        System.out.println(e.getMessage());
      }
      System.out.println(INSTRUCTION_MESSAGE);
      input = scn.next();
    }
  }

  private static void printWelcomeMessage() {
    System.out.println("**************************************************************");
    System.out.println("**  This program will convert your numeric input to letters **");
    System.out.println("**************************************************************");
    System.out.println(INSTRUCTION_MESSAGE);
  }

}
